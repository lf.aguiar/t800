import { Module, OnModuleInit } from '@nestjs/common';
import { DiscordService } from './discord.service';
import { Client } from 'discord.js';

@Module({
  providers: [DiscordService],
})
export class DiscordModule implements OnModuleInit {
  constructor(private readonly discordService: DiscordService) {}

  onModuleInit() {
    const client = new Client();
    client.login('ODEzMTA2MTE1NzU5MDQ2Njc3.YDKeFA.myJCjAGz28p1JUK1Q2ZkQwkxPxo');

    client.on('ready', () => {
      console.log(`Logged in as ${client.user.tag}!`);
    });

    client.on('message', (msg) => {
      if (msg.content.includes('t800'))
        this.discordService.identifyCommand(msg);
    });
  }
}
