import { Injectable } from '@nestjs/common';
import { Message } from 'discord.js';

@Injectable()
export class DiscordService {
  identifyCommand(msg: Message): void {
    const { content, channel } = msg;

    if (content.includes('server up'))
      channel.send('Estou levantando o servidor, guenta ae');
  }
}
